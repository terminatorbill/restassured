package com.example.models;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TicketDraw {
    private Integer drawId;
    private List<String> games = Collections.emptyList();
    private LocalDateTime drawDate;
    private TicketOutcome outcome;

    public Integer getDrawId() {
        return drawId;
    }

    public void setDrawId(Integer drawId) {
        this.drawId = drawId;
    }

    public LocalDateTime getDrawDate() {
        return drawDate;
    }

    public void setDrawDate(LocalDateTime drawDate) {
        this.drawDate = drawDate;
    }

    public TicketOutcome getOutcome() {
        return outcome;
    }

    public void setOutcome(TicketOutcome outcome) {
        this.outcome = outcome;
    }

    public List<String> getGames() {
        return games;
    }

    public void setGames(List<String> games) {
        this.games = Objects.requireNonNull(games);
    }
}
