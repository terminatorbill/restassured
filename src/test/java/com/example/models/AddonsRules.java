package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class AddonsRules {
    private List<Addon> addons = Collections.emptyList();
    private List<AddonCombinations> addonCombinations = Collections.emptyList();
    private List<String> defaultPlayCombination = Collections.emptyList();

    public List<Addon> getAddons() {
        return addons;
    }

    public void setAddons(List<Addon> addons) {
        this.addons = Objects.requireNonNull(addons);
    }

    public List<AddonCombinations> getAddonCombinations() {
        return addonCombinations;
    }

    public void setAddonCombinations(List<AddonCombinations> addonCombinations) {
        this.addonCombinations = Objects.requireNonNull(addonCombinations);
    }

    public List<String> getDefaultPlayCombination() {
        return defaultPlayCombination;
    }

    public void setDefaultPlayCombination(List<String> defaultPlayCombination) {
        this.defaultPlayCombination = defaultPlayCombination;
    }
}
