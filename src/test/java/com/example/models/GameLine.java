package com.example.models;

import java.util.List;
import java.util.Map;

public interface GameLine {
    Map<String, List<Object>> getDrawMatrices();
}
