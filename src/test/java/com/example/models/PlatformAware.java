package com.example.models;

import java.util.List;

public interface PlatformAware {
    List<String> getPlatforms();
}
