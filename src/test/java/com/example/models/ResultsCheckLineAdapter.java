package com.example.models;

import java.util.List;
import java.util.Map;

public class ResultsCheckLineAdapter implements GameLine {

    private ResultsCheckLine resultsCheckLine;

    public ResultsCheckLineAdapter(ResultsCheckLine resultsCheckLine) {
        this.resultsCheckLine = resultsCheckLine;
    }

    @Override
    public Map<String, List<Object>> getDrawMatrices() {
        if (resultsCheckLine.getPools() == null) {
            return null;
        }
        return resultsCheckLine.getPools().getDrawMatrices();
    }
}
