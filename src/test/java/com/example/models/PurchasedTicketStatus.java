package com.example.models;

public enum PurchasedTicketStatus {
    NOT_WINNING,
    PAID,
    WINNING,
    NOT_COMPLETED,
    CANCELLED,
    BLOCKED
}
