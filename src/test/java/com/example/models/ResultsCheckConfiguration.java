package com.example.models;

public class ResultsCheckConfiguration {
    private Integer maxLinesToCheck;
    private Integer maxDraws;

    public Integer getMaxLinesToCheck() {
        return maxLinesToCheck;
    }

    public void setMaxLinesToCheck(Integer maxLinesToCheck) {
        this.maxLinesToCheck = maxLinesToCheck;
    }

    public Integer getMaxDraws() {
        return maxDraws;
    }

    public void setMaxDraws(Integer maxDraws) {
        this.maxDraws = maxDraws;
    }
}
