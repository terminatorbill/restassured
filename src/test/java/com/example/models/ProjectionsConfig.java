package com.example.models;

public class ProjectionsConfig {
    private Long amount;
    private Boolean guaranteed = Boolean.TRUE;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Boolean getGuaranteed() {
        return guaranteed;
    }

    public void setGuaranteed(Boolean guaranteed) {
        this.guaranteed = guaranteed;
    }
}
