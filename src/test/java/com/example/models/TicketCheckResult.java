package com.example.models;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TicketCheckResult {

    private String gameId;
    private List<TicketStatus> ticketStatus = Collections.emptyList();
    private Integer startDraw;
    private Integer endDraw;
    private Integer remainingDraws;
    private List<TicketCheckPrizeTier> prizes = Collections.emptyList();
    private LocalDate paymentDeadline;
    private WinningTier winningTier;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<TicketStatus> getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(List<TicketStatus> ticketStatus) {
        this.ticketStatus = Objects.requireNonNull(ticketStatus);
    }

    public Integer getStartDraw() {
        return startDraw;
    }

    public void setStartDraw(Integer startDraw) {
        this.startDraw = startDraw;
    }

    public Integer getEndDraw() {
        return endDraw;
    }

    public void setEndDraw(Integer endDraw) {
        this.endDraw = endDraw;
    }

    public Integer getRemainingDraws() {
        return remainingDraws;
    }

    public void setRemainingDraws(Integer remainingDraws) {
        this.remainingDraws = remainingDraws;
    }

    public List<TicketCheckPrizeTier> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<TicketCheckPrizeTier> prizes) {
        this.prizes = Objects.requireNonNull(prizes);
    }

    public LocalDate getPaymentDeadline() {
        return paymentDeadline;
    }

    public void setPaymentDeadline(LocalDate paymentDeadline) {
        this.paymentDeadline = paymentDeadline;
    }

    public WinningTier getWinningTier() {
        return winningTier;
    }

    public void setWinningTier(WinningTier winningTier) {
        this.winningTier = winningTier;
    }
}
