package com.example.models;

public class QuickTipRequest {
    private QuickTipLine line = new QuickTipLine();
    private QuickTipLineHints hints = new QuickTipLineHints();

    public QuickTipLine getLine() {
        return line;
    }

    public void setLine(QuickTipLine line) {
        this.line = line;
    }

    public QuickTipLineHints getHints() {
        return hints;
    }

    public void setHints(QuickTipLineHints hints) {
        this.hints = hints;
    }
}
