package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ResultCheckResult {
    private List<ResultCheckDrawResult> draws = Collections.emptyList();

    public List<ResultCheckDrawResult> getDraws() {
        return draws;
    }

    public void setDraws(List<ResultCheckDrawResult> draws) {
        this.draws = Objects.requireNonNull(draws);
    }
}
