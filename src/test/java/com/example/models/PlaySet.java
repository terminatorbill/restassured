package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class PlaySet {
    private List<Integer> selections = Collections.emptyList();
    private List<SelectionType> selectionTypes = Collections.emptyList();

    public List<Integer> getSelections() {
        return selections;
    }

    public void setSelections(List<Integer> selections) {
        this.selections = Objects.requireNonNull(selections);
    }

    public List<SelectionType> getSelectionTypes() {
        return selectionTypes;
    }

    public void setSelectionTypes(List<SelectionType> selectionTypes) {
        this.selectionTypes = Objects.requireNonNull(selectionTypes);
    }
}
