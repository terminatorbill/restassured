package com.example.models;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public enum Channel {
    MOBILE(3),
    WEB(1);

    private static Map<Integer, Channel> map;

    private final int id;

    Channel(int id) {
        this.id = id;
    }

    static {
        ImmutableMap.Builder<Integer, Channel> builder = ImmutableMap.builder();
        for (Channel channel : values()) {
            builder.put(channel.getChannelId(), channel);
        }
        map = builder.build();
    }

    public static Channel fromValue(Integer code) {
        return map.getOrDefault(code, null);
    }

    public int getChannelId() {
        return id;
    }

    public static Channel fromValue(int id) {
        return map.get(id);
    }

}
