package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class QuickTipLine implements GameLine {
    private Map<String, List<Object>> drawMatrices = Collections.emptyMap();

    @Override
    public Map<String, List<Object>> getDrawMatrices() {
        return drawMatrices;
    }

    public void setDrawMatrices(Map<String, List<Object>> drawSets) {
        this.drawMatrices = Objects.requireNonNull(drawSets);
    }
}
