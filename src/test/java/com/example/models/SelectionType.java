package com.example.models;

public enum SelectionType {
    GENERATED,
    PLAYER_SELECTION,
    PROPOSED
}
