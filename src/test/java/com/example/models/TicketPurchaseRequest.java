package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TicketPurchaseRequest {
    private int draws;
    private List<ResultsCheckLine> lines = Collections.emptyList();
    private Long price;

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public List<ResultsCheckLine> getLines() {
        return lines;
    }

    public void setLines(List<ResultsCheckLine> lines) {
        this.lines = Objects.requireNonNull(lines);
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
