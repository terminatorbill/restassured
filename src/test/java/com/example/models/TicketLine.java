package com.example.models;

import java.util.Objects;

public class TicketLine {
    private TicketResultsLine line;
    private boolean quickPick;

    public TicketResultsLine getLine() {
        return line;
    }

    public void setLine(TicketResultsLine line) {
        this.line = Objects.requireNonNull(line);
    }

    public boolean isQuickPick() {
        return quickPick;
    }

    public void setQuickPick(boolean quickPick) {
        this.quickPick = quickPick;
    }
}
