package com.example.models;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Rule implements RangeObject<LocalDate> {
    private LocalDate validFrom;
    private LocalDate validTo;
    private Integer claimPeriodDays;
    private WageringRules wageringRules;
    private List<DrawDay> drawDays = Collections.emptyList();
    private MatrixRules matrixRules;
    private DrawSchedule drawSchedule;
    private AddonsRules addonsRules;
    private AddonsRules ticketAddonsRules;

    @Override
    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    @Override
    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    public Integer getClaimPeriodDays() {
        return claimPeriodDays;
    }

    public void setClaimPeriodDays(Integer claimPeriodDays) {
        this.claimPeriodDays = claimPeriodDays;
    }

    public WageringRules getWageringRules() {
        return wageringRules;
    }

    public void setWageringRules(WageringRules wageringRules) {
        this.wageringRules = wageringRules;
    }

    public MatrixRules getMatrixRules() {
        return matrixRules;
    }

    public void setMatrixRules(MatrixRules matrixRules) {
        this.matrixRules = matrixRules;
    }

    public DrawSchedule getDrawSchedule() {
        return drawSchedule;
    }

    public void setDrawSchedule(DrawSchedule drawSchedule) {
        this.drawSchedule = drawSchedule;
    }

    public AddonsRules getAddonsRules() {
        return addonsRules;
    }

    public void setAddonsRules(AddonsRules addonsRules) {
        this.addonsRules = addonsRules;
    }

    public List<DrawDay> getDrawDays() {
        return drawDays;
    }

    public void setDrawDays(List<DrawDay> drawDays) {
        this.drawDays = Objects.requireNonNull(drawDays);
    }

    public AddonsRules getTicketAddonsRules() {
        return ticketAddonsRules;
    }

    public void setTicketAddonsRules(AddonsRules ticketAddonsRules) {
        this.ticketAddonsRules = ticketAddonsRules;
    }
}
