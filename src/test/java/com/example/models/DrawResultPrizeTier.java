package com.example.models;

public class DrawResultPrizeTier {
    private String match;
    private Long prize;
    private String prizeType;
    private Long numberOfWinners;
    private Long numberOfNationalWinners;

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public Long getPrize() {
        return prize;
    }

    public void setPrize(Long prize) {
        this.prize = prize;
    }

    public String getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(String prizeType) {
        this.prizeType = prizeType;
    }

    public Long getNumberOfWinners() {
        return numberOfWinners;
    }

    public void setNumberOfWinners(Long numberOfWinners) {
        this.numberOfWinners = numberOfWinners;
    }

    public Long getNumberOfNationalWinners() {
        return numberOfNationalWinners;
    }

    public void setNumberOfNationalWinners(Long numberOfNationalWinners) {
        this.numberOfNationalWinners = numberOfNationalWinners;
    }
}
