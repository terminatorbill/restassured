package com.example.models;

public enum TicketStatus {
    LOST,
    PAID,
    UNPAID,
    NOT_COMPLETED,
    EXPIRED,
    PHONE_RISK
}
