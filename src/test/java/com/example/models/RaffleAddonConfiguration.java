package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class RaffleAddonConfiguration {
    private List<String> drawnIndependently = Collections.emptyList();
    private DisplayOption display;

    public List<String> getDrawnIndependently() {
        return drawnIndependently;
    }

    public void setDrawnIndependently(List<String> drawnIndependently) {
        this.drawnIndependently = Objects.requireNonNull(drawnIndependently);
    }

    public DisplayOption getDisplay() {
        return display;
    }

    public void setDisplay(DisplayOption display) {
        this.display = display;
    }
}
