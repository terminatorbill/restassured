package com.example.models;

public enum ErrorCode {
    OPERATION_NOT_SUPPORTED,
    GAME_DOES_NOT_EXIST,
    DRAW_RESULT_DOES_NOT_EXIST,
    NO_DRAW_RESULTS,
    OFFSET_DRAW_ID_NOT_EXIST,
    INVALID_MAX_DRAWS,
    INVALID_MAX_DAYS,
    INVALID_NUMBERS,
    INVALID_MATRIX,
    INVALID_MATRIX_SELECTION_TYPE,
    INVALID_NUMBER_OF_PLUS_GAMES,
    INVALID_PLAYSET,
    INVALID_QUICK_PICKS,
    INVALID_WAGERING_COST,
    TICKET_BLOCKED,
    TICKET_CANCELLED,
    TICKET_DOES_NOT_EXIST,
    DRAW_STILL_ACTIVE,
    RESULTS_UNKNOWN,
    GENERIC_ERROR,
    UNKNOWN_TICKET_STATUS,
    NOT_ALLOWED_COUNTRY,
    NOT_ALLOWED,
    NOT_ENOUGH_FUNDS,
    SPENDING_LIMITS_EXCEEDED,
    DRAW_BREAK,
    STALE_DRAW_ID,
    UNKNOWN_DRAW_STATUS,
    ADDONS_NOT_SOLD_INDEPENDENTLY,
    INVALID_LINES,
    INVALID_DRAWS,
    INVALID_REQUESTED_DAYS,
    OFFLINE_TICKET_SCANNING,
    DBG_WAGER_UNAVAILABLE,
    NOT_SUPPORTED_SYMBOL,
    /**
     * when you pass a line with more or less numbers that the expected
     */
    INVALID_NUMBER_OF_NUMBERS,
    /** When the saved playslip contain less lines than the min, or more than the max allowed */
    INVALID_NUMBER_OF_LINES,
    /**
     * when a playslip contains illegal entries i.e. :
     * <ul>
     *     <li>
     *      an out of range number is passed e.g. <u><b>-1 or 100</b></u> when a lotto number falls into <u><b>[1, 47]</b></u>
     *     </li>
     *     <li>
     *      a playslip contains duplicate numbers e.g. [<u><b>1,1</b></u>,2,3,4,5]
     *     </li>
     * </ul>
     */
    INVALID_NUMBER_RANGE,
    /**
     * When you have reached the max allowed saved playslips for the game
     */
    MAX_FAVORITES_PER_GAME_REACHED,
    /**
     * When another saved playslip has the same name. The uniqueness of the name does not apply to
     * playslips of the same game, but across all saved playslips for that player (regardless of the game)
     */
    FAVORITE_NAME_ALREADY_EXISTS,
    /**
     * a saved playslip should apply to a single main-game.
     * Thrown when:
     * <ol>
     *     <li>the passed games of a single line contain addon-games of other main-games or directly other main-games</li>
     *     <li>there are two lines within the same playslip that apply to different main-games</li>
     * </ol>
     */
    FAVORITE_PLAYSLIP_APPLIED_TO_DIFFERENT_GAMES,
    /**
     * Thrown when a line within the playslip is missing the main-game id.
     */
    FAVORITE_PLAYSLIP_IS_MISSING_MAIN_GAME,
    /**
     * Thrown when the playslip is saved with addons(excluding raffles) but some addons are missing.
     */
    FAVORITE_PLAYSLIP_IS_MISSING_ADDON_GAME,
    /**
     * Thrown when the updated favorite was not found
     */
    FAVORITE_DOES_NOT_EXIST,
    /**
     * Thrown when trying to save a favorite with:<br>
     * <ol>
     *  <li>quickpick is selected and pools are also passed</li>
     *  <li>quickpick is not-selected and pools are missing</li>
     * </ol>
     *
     */
    FAVORITE_ILLEGAL_QUICKPICK,
    /**
     * Thrown when trying to save or get favorites for a game that does not support favorites.
     */
    FAVORITE_NOT_SUPPORTED_FOR_GAME,
    /**
     * Thrown when retrieving all favorite playslips for a game-id
     */
    GAME_ID_QUERY_PARAM_IS_MISSING,
    MAX_DRAWS_AND_MAX_DAYS,
    SOLD_OUT
}
