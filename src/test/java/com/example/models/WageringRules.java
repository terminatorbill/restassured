package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class WageringRules {
    private boolean qpLine;
    private int minLines;
    private int maxLines;
    private List<Integer> multiplies = Collections.emptyList();
    private boolean soldIndependently;
    private List<Integer> allowedNumberOfDraws = Collections.emptyList();
    private List<Countdown> countdown = Collections.emptyList();

    public boolean isQpLine() {
        return qpLine;
    }

    public void setQpLine(boolean qpLine) {
        this.qpLine = qpLine;
    }

    public int getMinLines() {
        return minLines;
    }

    public void setMinLines(int minLines) {
        this.minLines = minLines;
    }

    public int getMaxLines() {
        return maxLines;
    }

    public void setMaxLines(int maxLines) {
        this.maxLines = maxLines;
    }

    public List<Integer> getMultiplies() {
        return multiplies;
    }

    public void setMultiplies(List<Integer> multiplies) {
        this.multiplies = Objects.requireNonNull(multiplies);
    }

    public boolean isSoldIndependently() {
        return soldIndependently;
    }

    public void setSoldIndependently(boolean soldIndependently) {
        this.soldIndependently = soldIndependently;
    }

    public List<Integer> getAllowedNumberOfDraws() {
        return allowedNumberOfDraws;
    }

    public void setAllowedNumberOfDraws(List<Integer> allowedNumberOfDraws) {
        this.allowedNumberOfDraws = Objects.requireNonNull(allowedNumberOfDraws);
    }

    public List<Countdown> getCountdown() {
        return countdown;
    }

    public void setCountdown(List<Countdown> countdown) {
        this.countdown = countdown;
    }
}
