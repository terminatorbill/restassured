package com.example.models;

import java.util.Collections;
import java.util.List;

public class TicketOutcome {
    private DrawStatus drawStatus;
    private List<TicketParticipationStatus> participationStatus = Collections.emptyList();
    private List<Prize> prizes = Collections.emptyList();

    public DrawStatus getDrawStatus() {
        return drawStatus;
    }

    public void setDrawStatus(DrawStatus drawStatus) {
        this.drawStatus = drawStatus;
    }

    public List<TicketParticipationStatus> getParticipationStatus() {
        return participationStatus;
    }

    public void setParticipationStatus(List<TicketParticipationStatus> participationStatus) {
        this.participationStatus = participationStatus;
    }

    public List<Prize> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<Prize> prizes) {
        this.prizes = prizes;
    }
}
