package com.example.models;

import java.time.LocalDateTime;

public class Countdown implements RangeObject<LocalDateTime> {

    private LocalDateTime validFrom;
    private LocalDateTime validTo;
    private Integer maxNumberOfDraws;

    @Override
    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    @Override
    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    public Integer getMaxNumberOfDraws() {
        return maxNumberOfDraws;
    }

    public void setMaxNumberOfDraws(Integer maxNumberOfDraws) {
        this.maxNumberOfDraws = maxNumberOfDraws;
    }
}
