package com.example.models;

public interface RangeObject<T extends Comparable<?>> {

    T getValidFrom();

    T getValidTo();

}
