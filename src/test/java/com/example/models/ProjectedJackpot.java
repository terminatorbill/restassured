package com.example.models;

import java.time.LocalDateTime;

public class ProjectedJackpot {
    private String gameId;
    private LocalDateTime nextDrawDate;
    private Long amount;
    private Boolean guaranteed;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public LocalDateTime getNextDrawDate() {
        return nextDrawDate;
    }

    public void setNextDrawDate(LocalDateTime nextDrawDate) {
        this.nextDrawDate = nextDrawDate;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Boolean getGuaranteed() {
        return guaranteed;
    }

    public void setGuaranteed(Boolean guaranteed) {
        this.guaranteed = guaranteed;
    }

}
