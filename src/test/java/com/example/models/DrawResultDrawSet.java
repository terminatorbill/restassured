package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class DrawResultDrawSet {
    private List<DrawResultDrawNumber> drawNumbers = Collections.emptyList();

    public List<DrawResultDrawNumber> getDrawNumbers() {
        return drawNumbers;
    }

    public void setDrawNumbers(List<DrawResultDrawNumber> drawNumbers) {
        this.drawNumbers = Objects.requireNonNull(drawNumbers);
    }
}
