package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class AddonCombinations {
    private List<String> addons = Collections.emptyList();
    private Long price;

    public List<String> getAddons() {
        return addons;
    }

    public void setAddons(List<String> addons) {
        this.addons = Objects.requireNonNull(addons);
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
