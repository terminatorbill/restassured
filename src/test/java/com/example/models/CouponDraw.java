package com.example.models;

import java.time.LocalDateTime;

public class CouponDraw {
    private Integer drawId;
    private LocalDateTime drawDate;
    private CouponOutcome outcome;

    public Integer getDrawId() {
        return drawId;
    }

    public void setDrawId(Integer drawId) {
        this.drawId = drawId;
    }

    public LocalDateTime getDrawDate() {
        return drawDate;
    }

    public void setDrawDate(LocalDateTime drawDate) {
        this.drawDate = drawDate;
    }

    public CouponOutcome getOutcome() {
        return outcome;
    }

    public void setOutcome(CouponOutcome outcome) {
        this.outcome = outcome;
    }
}
