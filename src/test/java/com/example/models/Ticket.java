package com.example.models;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Ticket {
    private String ticketId;
    private List<TicketLine> lines = Collections.emptyList();
    private List<String> games = Collections.emptyList();
    private List<PurchasedTicketStatus> ticketStatus = Collections.emptyList();
    private Long price;
    private LocalDateTime playedOn;
    private String gameId;
    private List<TicketDraw> draws = Collections.emptyList();
    private Channel channel;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public List<TicketLine> getLines() {
        return lines;
    }

    public void setLines(List<TicketLine> lines) {
        this.lines = Objects.requireNonNull(lines);
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public LocalDateTime getPlayedOn() {
        return playedOn;
    }

    public void setPlayedOn(LocalDateTime playedOn) {
        this.playedOn = playedOn;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<PurchasedTicketStatus> getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(List<PurchasedTicketStatus> ticketStatus) {
        this.ticketStatus = Objects.requireNonNull(ticketStatus);
    }

    public List<TicketDraw> getDraws() {
        return draws;
    }

    public void setDraws(List<TicketDraw> draws) {
        this.draws = Objects.requireNonNull(draws);
    }

    public void setGames(List<String> games) {
        this.games = Objects.requireNonNull(games);
    }

    public List<String> getGames() {
        return games;
    }
}
