package com.example.models;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class AllGames {
    private Map<String, Game> games = Collections.emptyMap();
    private String dbgVersion;

    public Map<String, Game> getGames() {
        return games;
    }

    public void setGames(Map<String, Game> games) {
        this.games = Objects.requireNonNull(games);
    }

    public String getDbgVersion() {
        return dbgVersion;
    }

    public void setDbgVersion(String dbgVersion) {
        this.dbgVersion = dbgVersion;
    }
}
