package com.example.models;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DrawResult {
    private String gameId;
    private LocalDateTime drawDate;
    private LocalDateTime nextDrawDate;
    private Integer drawId;
    private DrawStatus status;
    private String machine;
    private String ballSet;
    private String message;
    private Long topPrize;
    private List<DrawResultPrizeTier> prizes = Collections.emptyList();
    private List<Map<String, DrawResultMatrix>> drawnLines = Collections.emptyList();
    private Map<String, DrawResult> addonGames = Collections.emptyMap();

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public LocalDateTime getDrawDate() {
        return drawDate;
    }

    public void setDrawDate(LocalDateTime drawDate) {
        this.drawDate = drawDate;
    }

    public LocalDateTime getNextDrawDate() {
        return nextDrawDate;
    }

    public void setNextDrawDate(LocalDateTime nextDrawDate) {
        this.nextDrawDate = nextDrawDate;
    }

    public Integer getDrawId() {
        return drawId;
    }

    public void setDrawId(Integer drawId) {
        this.drawId = drawId;
    }

    public DrawStatus getStatus() {
        return status;
    }

    public void setStatus(DrawStatus status) {
        this.status = status;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public String getBallSet() {
        return ballSet;
    }

    public void setBallSet(String ballSet) {
        this.ballSet = ballSet;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTopPrize() {
        return topPrize;
    }

    public void setTopPrize(Long topPrize) {
        this.topPrize = topPrize;
    }

    public List<DrawResultPrizeTier> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<DrawResultPrizeTier> prizes) {
        this.prizes = Objects.requireNonNull(prizes);
    }

    public List<Map<String, DrawResultMatrix>> getDrawnLines() {
        return drawnLines;
    }

    public void setDrawnLines(List<Map<String, DrawResultMatrix>> drawnLines) {
        this.drawnLines = drawnLines;
    }

    public Map<String, DrawResult> getAddonGames() {
        return addonGames;
    }

    public void setAddonGames(Map<String, DrawResult> addonGames) {
        this.addonGames = Objects.requireNonNull(addonGames);
    }


}
