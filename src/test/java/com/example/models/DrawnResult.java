package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DrawnResult {

    private Map<String, DrawResultMatrix> drawnLine = Collections.emptyMap();
    private Map<String, List<CheckResultPrizeTier>> wonPrizes = Collections.emptyMap();

    public Map<String, DrawResultMatrix> getDrawnLine() {
        return drawnLine;
    }

    public void setDrawnLine(Map<String, DrawResultMatrix> drawnLine) {
        this.drawnLine = Objects.requireNonNull(drawnLine);
    }

    public Map<String, List<CheckResultPrizeTier>> getWonPrizes() {
        return wonPrizes;
    }

    public void setWonPrizes(Map<String, List<CheckResultPrizeTier>> wonPrizes) {
        this.wonPrizes = Objects.requireNonNull(wonPrizes);
    }
}
