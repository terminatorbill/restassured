package com.example.models;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class HistoryTicket {
    private String ticketId;
    private String gameId;
    private Long price;
    private LocalDateTime playedOn;
    private Channel channel;
    private List<PurchasedTicketStatus> ticketStatus = Collections.emptyList();
    private Prize prize;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public LocalDateTime getPlayedOn() {
        return playedOn;
    }

    public void setPlayedOn(LocalDateTime playedOn) {
        this.playedOn = playedOn;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<PurchasedTicketStatus> getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(List<PurchasedTicketStatus> ticketStatus) {
        this.ticketStatus = Objects.requireNonNull(ticketStatus);
    }

    public Prize getPrize() {
        return prize;
    }

    public void setPrize(Prize prize) {
        this.prize = prize;
    }
}
