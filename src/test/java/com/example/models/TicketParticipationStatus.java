package com.example.models;

public enum TicketParticipationStatus {
    ENTERED, WINNING, NOT_WINNING, PAID
}
