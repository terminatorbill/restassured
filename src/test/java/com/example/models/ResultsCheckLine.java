package com.example.models;

import java.util.Collections;
import java.util.List;

/**
 * This class models the player's selected "numbers" submitted in a game's (plus addon games) lottery draw.
 */
public class ResultsCheckLine {
    /**
     * The ids of this line's games.
     * Handles the case of submitting the same line for multiple games at the same time
     * e.g the main along with its addons games
     */
    private List<String> games = Collections.emptyList();
    private ResultCheckMatrix pools;
    private boolean quickPick;

    public ResultCheckMatrix getPools() {
        return pools;
    }

    public void setPools(ResultCheckMatrix pools) {
        this.pools = pools;
    }

    /**
     * @return the games' ids of this played line
     */
    public List<String> getGames() {
        return games;
    }

    public void setGames(List<String> games) {
        this.games = games;
    }

    public boolean isQuickPick() {
        return quickPick;
    }

    public void setQuickPick(boolean quickPick) {
        this.quickPick = quickPick;
    }
}
