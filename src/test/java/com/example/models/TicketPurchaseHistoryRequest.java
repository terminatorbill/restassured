package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TicketPurchaseHistoryRequest {
    private List<String> games = Collections.emptyList();

    public List<String> getGames() {
        return games;
    }

    public void setGames(List<String> games) {
        this.games = Objects.requireNonNull(games);
    }
}
