package com.example.models;

public enum SymbolType {
    NUMBER,
    LETTER
}
