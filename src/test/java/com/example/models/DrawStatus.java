package com.example.models;

public enum DrawStatus {
    CONFIRMED, UNCONFIRMED, OPEN, CLOSED, PROCESSED, DRAW_BREAK, PENDING
}
