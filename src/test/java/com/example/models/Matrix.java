package com.example.models;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class Matrix {
    private PlaySet playSet;
    private int size;
    private ValueRange values;
    private Map<String, DrawSet> drawSets = Collections.emptyMap();

    public PlaySet getPlaySet() {
        return playSet;
    }

    public void setPlaySet(PlaySet playSet) {
        this.playSet = playSet;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ValueRange getValues() {
        return values;
    }

    public void setValues(ValueRange values) {
        this.values = values;
    }

    public Map<String, DrawSet> getDrawSets() {
        return drawSets;
    }

    public void setDrawSets(Map<String, DrawSet> drawSets) {
        this.drawSets = Objects.requireNonNull(drawSets);
    }
}
