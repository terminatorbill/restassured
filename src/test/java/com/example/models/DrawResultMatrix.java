package com.example.models;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class DrawResultMatrix {

    private Map<String, DrawResultDrawSet> drawSets = Collections.emptyMap();

    public Map<String, DrawResultDrawSet> getDrawSets() {
        return drawSets;
    }

    public void setDrawSets(Map<String, DrawResultDrawSet> drawSets) {
        this.drawSets = Objects.requireNonNull(drawSets);
    }
}
