package com.example.models;

public class RaffleResult {
    private Integer drawId;
    private String winningId;
    private String message;

    public Integer getDrawId() {
        return drawId;
    }

    public void setDrawId(Integer drawId) {
        this.drawId = drawId;
    }

    public String getWinningId() {
        return winningId;
    }

    public void setWinningId(String winningId) {
        this.winningId = winningId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
