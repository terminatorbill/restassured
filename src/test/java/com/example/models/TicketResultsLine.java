package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TicketResultsLine {
    private List<String> games = Collections.emptyList();
    private ResultCheckMatrix pools;

    public ResultCheckMatrix getPools() {
        return pools;
    }

    public void setPools(ResultCheckMatrix pools) {
        this.pools = pools;
    }

    public List<String> getGames() {
        return games;
    }

    public void setGames(List<String> games) {
        this.games =  Objects.requireNonNull(games);
    }
}
