package com.example.models;

import java.time.DayOfWeek;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ResultCheckRequest {
    private List<DayOfWeek> drawDays = Collections.emptyList();
    private List<ResultsCheckLine> lines = Collections.emptyList();

    public List<ResultsCheckLine> getLines() {
        return lines;
    }

    public void setLines(List<ResultsCheckLine> lines) {
        this.lines = Objects.requireNonNull(lines);
    }

    public List<DayOfWeek> getDrawDays() {
        return drawDays;
    }

    public void setDrawDays(List<DayOfWeek> drawDays) {
        this.drawDays = Objects.requireNonNull(drawDays);
    }

}
