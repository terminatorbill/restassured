package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Game implements PlatformAware {
    private String id;
    private String displayName;
    private String codeName;
    private String timeZoneId;
    private boolean raffle;

    private List<String> platforms = Collections.emptyList();
    private List<Rule> rules = Collections.emptyList();
    private ViewConfiguration viewConfiguration;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public boolean isRaffle() {
        return raffle;
    }

    public void setRaffle(boolean raffle) {
        this.raffle = raffle;
    }

    @Override
    public List<String> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = Objects.requireNonNull(rules);
    }

    public ViewConfiguration getViewConfiguration() {
        return viewConfiguration;
    }

    public void setViewConfiguration(ViewConfiguration viewConfiguration) {
        this.viewConfiguration = viewConfiguration;
    }
}
