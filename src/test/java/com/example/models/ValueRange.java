package com.example.models;

import java.util.List;
import java.util.Objects;

public class ValueRange {
    private List<Object> range;
    private SymbolType symbolType;

    public List<Object> getRange() {
        return range;
    }

    public void setRange(List<Object> range) {
        this.range = Objects.requireNonNull(range);
    }

    public SymbolType getSymbolType() {
        return symbolType;
    }

    public void setSymbolType(SymbolType symbolType) {
        this.symbolType = symbolType;
    }
}
