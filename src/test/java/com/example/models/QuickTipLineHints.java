package com.example.models;

import java.util.Collections;
import java.util.Map;

public class QuickTipLineHints {
    private Map<String, Integer> playSetSelection = Collections.emptyMap();

    public Map<String, Integer> getPlaySetSelection() {
        return playSetSelection;
    }

    public void setPlaySetSelection(Map<String, Integer> playSetSelection) {
        this.playSetSelection = playSetSelection;
    }
}
