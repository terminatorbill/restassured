package com.example.models;

public class DrawResultDrawNumber {
    private Object number;
    private Integer drawIndex;

    public Object getNumber() {
        return number;
    }

    public void setNumber(Object number) {
        this.number = number;
    }

    public Integer getDrawIndex() {
        return drawIndex;
    }

    public void setDrawIndex(Integer drawIndex) {
        this.drawIndex = drawIndex;
    }
}
