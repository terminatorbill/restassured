package com.example.models;

public enum PrizeType {
    CASH("Cash"),
    SCRATCHCARD("ScratchCard"),
    REPLAY("Replay");

    private final String value;

    PrizeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }
}
