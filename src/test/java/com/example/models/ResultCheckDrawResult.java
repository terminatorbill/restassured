package com.example.models;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class ResultCheckDrawResult {
    private LocalDateTime drawDate;
    private Integer drawId;
    private List<DrawnResult> drawnResults = Collections.emptyList();
    private Map<String, ResultCheckDrawResult> addonGames = Collections.emptyMap();

    public LocalDateTime getDrawDate() {
        return drawDate;
    }

    public void setDrawDate(LocalDateTime drawDate) {
        this.drawDate = drawDate;
    }

    public Integer getDrawId() {
        return drawId;
    }

    public void setDrawId(Integer drawId) {
        this.drawId = drawId;
    }

    public Map<String, ResultCheckDrawResult> getAddonGames() {
        return addonGames;
    }

    public void setAddonGames(Map<String, ResultCheckDrawResult> addonGames) {
        this.addonGames = addonGames;
    }

    public List<DrawnResult> getDrawnResults() {
        return drawnResults;
    }

    public void setDrawnResults(List<DrawnResult> drawnResults) {
        this.drawnResults = Objects.requireNonNull(drawnResults);
    }
}
