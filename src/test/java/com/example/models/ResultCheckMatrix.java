package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ResultCheckMatrix {

    private Map<String, List<Object>> drawMatrices = Collections.emptyMap();

    /**
     * Used to model cases where where a {@link ResultsCheckLine} consists of multiple matrices
     * @return Key = matrix type e.g.{"Main", "Extra"}, Value = selected "numbers"
     */
    public Map<String, List<Object>> getDrawMatrices() {
        return drawMatrices;
    }

    public void setDrawMatrices(Map<String, List<Object>> drawSets) {
        this.drawMatrices = Objects.requireNonNull(drawSets);
    }

}
