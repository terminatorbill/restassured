package com.example.models;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MatrixRules {
    private Map<String, Matrix> matrices = Collections.emptyMap();
    private List<String> matricesOrder = Collections.emptyList();
    private List<Map<String, SelectionType>> selectionCombinations = Collections.emptyList();
    private Map<String, List<String>> drawSetsOrder = Collections.emptyMap();
    private boolean sortByDrawNumber;

    public Map<String, Matrix> getMatrices() {
        return matrices;
    }

    public void setMatrices(Map<String, Matrix> matrices) {
        this.matrices = Objects.requireNonNull(matrices);
    }

    public List<String> getMatricesOrder() {
        return matricesOrder;
    }

    public void setMatricesOrder(List<String> matricesOrder) {
        this.matricesOrder = Objects.requireNonNull(matricesOrder);
    }

    public Map<String, List<String>> getDrawSetsOrder() {
        return drawSetsOrder;
    }

    public void setDrawSetsOrder(Map<String, List<String>> drawSetsOrder) {
        this.drawSetsOrder = Objects.requireNonNull(drawSetsOrder);
    }

    public boolean isSortByDrawNumber() {
        return sortByDrawNumber;
    }

    public void setSortByDrawNumber(boolean sortByDrawNumber) {
        this.sortByDrawNumber = sortByDrawNumber;
    }

    public List<Map<String, SelectionType>> getSelectionCombinations() {
        return selectionCombinations;
    }

    public void setSelectionCombinations(List<Map<String, SelectionType>> selectionCombinations) {
        this.selectionCombinations = Objects.requireNonNull(selectionCombinations);
    }
}
