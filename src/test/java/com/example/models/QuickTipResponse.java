package com.example.models;

public class QuickTipResponse {
    private QuickTipLine line;

    public QuickTipLine getLine() {
        return line;
    }

    public void setLine(QuickTipLine line) {
        this.line = line;
    }
}
