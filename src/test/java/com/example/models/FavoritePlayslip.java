package com.example.models;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FavoritePlayslip {
    /**
     * Uniquely identifies a favorite playslip
     */
    private String name;
    /**
     * Uniquely identifies a favorite playslip. Created by the external platform system.
     */
    private String id;

    /**
     * The datetime of the last update
     */
    private LocalDateTime lastUpdate;

    /**
     * The lines of this collection
     */
    private List<ResultsCheckLine> lines = Collections.emptyList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<ResultsCheckLine> getLines() {
        return lines;
    }

    public void setLines(List<ResultsCheckLine> lines) {
        this.lines = Objects.requireNonNull(lines);
    }
}
