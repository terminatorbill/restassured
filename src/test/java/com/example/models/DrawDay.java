package com.example.models;

import java.time.DayOfWeek;
import java.time.LocalTime;

public class DrawDay {
    private DayOfWeek day;
    private LocalTime time;
    private Long drawBreakOffset;
    private Long drawBreakDuration;
    private LocalTime salesCloseTime;
    private Long resultsOffset;

    public DayOfWeek getDay() {
        return day;
    }

    public void setDay(DayOfWeek day) {
        this.day = day;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Long getDrawBreakOffset() {
        return drawBreakOffset;
    }

    public void setDrawBreakOffset(Long drawBreakOffset) {
        this.drawBreakOffset = drawBreakOffset;
    }

    public Long getDrawBreakDuration() {
        return drawBreakDuration;
    }

    public void setDrawBreakDuration(Long drawBreakDuration) {
        this.drawBreakDuration = drawBreakDuration;
    }

    public LocalTime getSalesCloseTime() {
        return salesCloseTime;
    }

    public void setSalesCloseTime(LocalTime salesCloseTime) {
        this.salesCloseTime = salesCloseTime;
    }

    public Long getResultsOffset() {
        return resultsOffset;
    }

    public void setResultsOffset(Long resultsOffset) {
        this.resultsOffset = resultsOffset;
    }
}
