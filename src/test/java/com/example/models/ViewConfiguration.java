package com.example.models;

public class ViewConfiguration {
    private ResultsCheckConfiguration resultsCheckConfiguration;
    private ResultsViewConfiguration resultsViewConfiguration;
    private RaffleAddonConfiguration raffleAddonConfiguration;

    public ResultsCheckConfiguration getResultsCheckConfiguration() {
        return resultsCheckConfiguration;
    }

    public void setResultsCheckConfiguration(ResultsCheckConfiguration resultsCheckConfiguration) {
        this.resultsCheckConfiguration = resultsCheckConfiguration;
    }

    public ResultsViewConfiguration getResultsViewConfiguration() {
        return resultsViewConfiguration;
    }

    public void setResultsViewConfiguration(ResultsViewConfiguration resultsViewConfiguration) {
        this.resultsViewConfiguration = resultsViewConfiguration;
    }

    public RaffleAddonConfiguration getRaffleAddonConfiguration() {
        return raffleAddonConfiguration;
    }

    public void setRaffleAddonConfiguration(RaffleAddonConfiguration raffleAddonConfiguration) {
        this.raffleAddonConfiguration = raffleAddonConfiguration;
    }
}
