package com.example.models;

import java.util.Collections;
import java.util.List;

public class CouponOutcome {
    private DrawStatus status;
    private List<Prize> prizes = Collections.emptyList();

    public DrawStatus getStatus() {
        return status;
    }

    public void setStatus(DrawStatus status) {
        this.status = status;
    }

    public List<Prize> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<Prize> prizes) {
        this.prizes = prizes;
    }
}
