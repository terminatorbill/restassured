package com.example.auth;

import com.example.configuration.FunctionalTest;
import com.example.games.DbgGames;
import com.example.models.Credentials;
import io.restassured.http.ContentType;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class AuthTest extends FunctionalTest {

    @Test
    public void authenticate() {
        Map<String, String> credentials = new HashMap<>();
        credentials.put("username", "foo");
        credentials.put("password", "baz");

        given()
            .contentType(ContentType.JSON)
            .body(credentials)
                .when()
                    .post("/auth/login")
                .then()
                    .statusCode(200);
    }

    @Test
    public void authenticateJavaObject() {
        Credentials credentials = new Credentials();
        credentials.setUsername("foo");
        credentials.setPassword("baz");

        given()
            .contentType(ContentType.JSON)
            .body(credentials)
                .when()
                    .post("/auth/login")
            .then()
                .statusCode(200);
    }

    @Test
    public void authenticateJavaObjectFromFile() throws IOException {
        InputStream is = DbgGames.class.getResourceAsStream("/auth-request.json");
        Credentials credentials = objectMapper.readValue(is, Credentials.class);
        given()
            .contentType(ContentType.JSON)
            .body(credentials)
                .when()
                    .post("/auth/login")
            .then()
                .statusCode(200);
    }
}
