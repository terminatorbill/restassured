package com.example.configuration;

import com.example.deserializers.LocalDateDeserializer;
import com.example.deserializers.LocalDateTimeDeserializer;
import com.example.deserializers.LocalTimeDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import org.junit.BeforeClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static io.restassured.config.ObjectMapperConfig.objectMapperConfig;

public class FunctionalTest {
    public static final ObjectMapper objectMapper  = new ObjectMapper();

    static {
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(LocalDate.class, new LocalDateDeserializer());
        simpleModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
        simpleModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer());
        objectMapper.registerModule(simpleModule);
    }
    @BeforeClass
    public static void setup() {
        RestAssured.port = 8060;
        RestAssured.basePath = "/api/";

        String baseHost = System.getProperty("server.host");
        if(baseHost == null){
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;

        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(objectMapperConfig().jackson2ObjectMapperFactory(
                new Jackson2ObjectMapperFactory() {
                    @Override
                    public com.fasterxml.jackson.databind.ObjectMapper create(Class cls, String charset) {
                        return objectMapper;
                    }
                }
        ));
    }

}
