package com.example.games;

import com.example.configuration.FunctionalTest;
import com.example.models.AllGames;
import io.restassured.http.ContentType;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static io.restassured.RestAssured.given;

public class DbgGames extends FunctionalTest {

    private static final String X_CLIENT_VERSION = "X-CLIENT-VERSION";
    private static final String X_CLIENT_PLATFORM = "X-CLIENT-PLATFORM";

    @Test
    public void getGamesAssertCode() {
        given()
            .header(X_CLIENT_VERSION, "2.1.0")
            .header(X_CLIENT_PLATFORM, "ios")
            .when()
                .get("dbg/games")
            .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void getGamesAssertContentType() {
        given()
            .header("X-CLIENT-VERSION", "2.1.0")
            .header("X-CLIENT-PLATFORM", "ios")
                .when()
                    .get("dbg/games")
            .then()
                .assertThat()
                .contentType(ContentType.JSON);
    }

    @Test
    public void getGamesJavaObject() {
        AllGames allGames = given()
            .header("X-CLIENT-VERSION", "2.1.0")
            .header("X-CLIENT-PLATFORM", "ios")
                .when()
                    .get("dbg/games")
                    .as(AllGames.class);
        assert !allGames.getGames().isEmpty();
        assert !allGames.getGames().get("1").isRaffle();
        assert allGames.getGames().get("1").getCodeName().equals("lotto");
    }

    @Test
    public void getGamesCompareResponse() throws IOException {
        AllGames allGames = given()
            .header("X-CLIENT-VERSION", "2.1.0")
            .header("X-CLIENT-PLATFORM", "ios")
                .when()
                    .get("dbg/games")
                    .as(AllGames.class);
        InputStream is = DbgGames.class.getResourceAsStream("/games.json");
        /*String storedResponse = CharStreams.toString(new InputStreamReader(
                is, Charsets.UTF_8));*/

        AllGames storedResponse = objectMapper.readValue(is, AllGames.class);

        assert allGames.getDbgVersion().equals(storedResponse.getDbgVersion());
    }
}
